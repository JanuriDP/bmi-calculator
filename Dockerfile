FROM python:3.8-alpine

ARG user=bmiuser

RUN adduser -D $user 

USER $user 

COPY requirements.txt /usr/src

RUN pip install -r /usr/src/requirements.txt

COPY --chown=$user:$user ./app /usr/src/app

WORKDIR /usr/src/app

CMD ["python", "app.py"]