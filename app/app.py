from flask import Flask
from flask_restful import Api
from request import Request
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

sentry_sdk.init(
    dsn="https://abc57f72970d4cdcb99040f8fea7617a@o1296651.ingest.sentry.io/6526138",
    integrations=[
        FlaskIntegration(),
    ],

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)

app = Flask(__name__)
api = Api(app)

api.add_resource(Request, "/")

@app.route('/debug-sentry')
def trigger_error():
    division_by_zero = 1 / 0

if __name__ == '__main__':
   app.run('0.0.0.0','8000')
